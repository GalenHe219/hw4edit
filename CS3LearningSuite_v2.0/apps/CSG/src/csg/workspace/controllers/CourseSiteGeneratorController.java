/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace.controllers;

import csg.CourseSiteGeneratorApp;
import static csg.CourseSiteGeneratorPropertyType.CSG_FOOLPROOF_SETTINGS;
import static csg.CourseSiteGeneratorPropertyType.*;
import csg.data.CourseSiteGeneratorData;
import csg.data.Instructor;
import csg.data.TAType;
import csg.data.TeachingAssistantPrototype;
import csg.data.TimeSlot;
import csg.data.TimeSlot.DayOfWeek;
import csg.transactions.AddTA_Transaction;
import csg.transactions.*;
import csg.transactions.EditInstructorHours_Transaction;
import csg.transactions.EditTA_Transaction;
import csg.transactions.ToggleOfficeHours_Transaction;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.dialogs.AppDialogsFacade;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.json.JsonArray;

/**
 *
 * @author Galen
 */
public class CourseSiteGeneratorController {
    CourseSiteGeneratorApp app;
    
    public CourseSiteGeneratorController(CourseSiteGeneratorApp initApp){
        app = initApp;
    }
    
//TAB 1 FUCTIONALITY
    
    public void processChangeComboBox(){
        AppGUIModule gui = app.getGUIModule();
        ComboBox subjectsCB = (ComboBox) gui.getGUINode(CSG_SUBJECT_COMBO_BOX);
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        
        System.out.println("CHANGE PROCESSED");
        
        EditComboBox_Transaction editCB = new EditComboBox_Transaction(data);
        app.processTransaction(editCB);
    }
    
    public void processChangeTextField(){
        AppGUIModule gui = app.getGUIModule();
        TextField titleTF = (TextField) gui.getGUINode(CSG_TITLE_TEXT_FIELD);
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        
        EditComboBox_Transaction editCb = new EditComboBox_Transaction(data);
        app.processTransaction(editCb);
        
    }
    
    public void processMakeNewSubjectOption(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        ComboBox subjectsCB = (ComboBox) gui.getGUINode(CSG_SUBJECT_COMBO_BOX);
        ComboBox numbersCB = (ComboBox) gui.getGUINode(CSG_NUMBER_COMBO_BOX);
        
        ObservableList<String> subjectOptions = data.getSubjectOptions();
        //data.getSubjectOptions().getItems().add(newInput);
        String newInput = subjectsCB.getEditor().getText();
        if(!subjectOptions.contains(newInput)){
            //subjectsCB.getItems().addAll(newInput);
            subjectOptions.add(newInput);
        }
        
        
        
        
    }
    
    public void processMakeNewNumberOption(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        ComboBox subjectsCB = (ComboBox) gui.getGUINode(CSG_SUBJECT_COMBO_BOX);
        ComboBox numbersCB = (ComboBox) gui.getGUINode(CSG_NUMBER_COMBO_BOX);
        
        ObservableList<String> numberOptions = data.getNumberOptions();
        //data.getSubjectOptions().getItems().add(newInput);
        String newInput = numbersCB.getEditor().getText();
        if(!numberOptions.contains(newInput)){
            //numbersCB.getItems().addAll(newInput);
            numberOptions.add(newInput);
        }
        
    }
    
    public void processChangeExportDir(){
        AppGUIModule gui = app.getGUIModule();
        ComboBox subjectCB = (ComboBox) gui.getGUINode(CSG_SUBJECT_COMBO_BOX);
        ComboBox numberCB = (ComboBox) gui.getGUINode(CSG_NUMBER_COMBO_BOX);
        ComboBox semesterCB = (ComboBox) gui.getGUINode(CSG_SEMESTER_COMBO_BOX);
        ComboBox yearCB = (ComboBox) gui.getGUINode(CSG_YEAR_COMBO_BOX);
        
        String subject = subjectCB.getValue().toString();
        String number = numberCB.getValue().toString();
        String semester = semesterCB.getValue().toString();
        String year = yearCB.getValue().toString();
        
        String exportDir = ".\\export\\";
        exportDir = exportDir + subject + "_" + number + "_" + semester + "_" + year + "\\public_html";
        
        Label exportDirText = (Label) gui.getGUINode(CSG_EXPORT_DIR_LINK);
        exportDirText.setText(exportDir);
    }
    
    
//Pages Help Methods
    public void processChangeCheckBox(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        
        EditCheckBox_Transaction editChB = new EditCheckBox_Transaction(data);
        app.processTransaction(editChB);
        
    }
    
//Style Help methods
    public void processOpenFileChooser(){
        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(null);
        
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("Text Files", "*.txt"),
            new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
//        if(selectedFile != null){
//            actionStatus.setText(selectedFile.getName());
//        }
//        else{
//            actionStatus.setText("File selected canceled");
//        }
    }
    
    public void processChangeInstructorName(){
        AppGUIModule gui = app.getGUIModule();
        TextField textField = (TextField) gui.getGUINode(CSG_INSTRUCTOR_NAME_TEXT_FIELD);
        
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        Instructor guy = data.getInstructor();
        
        EditInstructorHours_Transaction editHours = new EditInstructorHours_Transaction(data, guy);
        app.processTransaction(editHours);
        
        //Delete this when transaction works
        data.getInstructor().setName(textField.getText());
        System.out.println("Guy's Name has been changed");

    }
    public void processChangeInstructorEmail(){
        AppGUIModule gui = app.getGUIModule();
        TextField textField = (TextField) gui.getGUINode(CSG_INSTRUCTOR_EMAIL_TEXT_FIELD);
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        Instructor guy = data.getInstructor();
        
        EditInstructorHours_Transaction editHours = new EditInstructorHours_Transaction(data, guy);
        app.processTransaction(editHours);
        
        //Delete this when transaction works
        data.getInstructor().setEmail(textField.getText());
        System.out.println("Guy's email has been changed");
    }
    public void processChangeInstructorRoom(){
        AppGUIModule gui = app.getGUIModule();
        TextField textField = (TextField) gui.getGUINode(CSG_INSTRUCTOR_ROOM_TEXT_FIELD);
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        Instructor guy = data.getInstructor();
        
        EditInstructorHours_Transaction editHours = new EditInstructorHours_Transaction(data, guy);
        app.processTransaction(editHours);
        
        //Delete this when transaction works
        data.getInstructor().setRoom(textField.getText());
        System.out.println("Guy's instructor has been changed");
    }
    public void processChangeInstructorHomePage(){
        AppGUIModule gui = app.getGUIModule();
        TextField textField = (TextField) gui.getGUINode(CSG_INSTRUCTOR_HOME_PAGE_TEXT_FIELD);
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        Instructor guy = data.getInstructor();
        
        EditInstructorHours_Transaction editHours = new EditInstructorHours_Transaction(data, guy);
        app.processTransaction(editHours);
        
        //Delete this when transaction works
        data.getInstructor().setHomePage(textField.getText());
        System.out.println("Guy's homepage has been changed");
    }
    
    public void processChangeInstructorHours(){
        AppGUIModule gui = app.getGUIModule();
        TextArea textArea = (TextArea) gui.getGUINode(CSG_INSTRUCTOR_OFFICE_HOURS);
        
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        Instructor guy = data.getInstructor();
        
        EditInstructorHours_Transaction editHours = new EditInstructorHours_Transaction(data, guy);
        app.processTransaction(editHours);
        
        //remove this once do and undo get implmeneted.
        data.getInstructor().setOfficeHours(textArea.getText());
        System.out.println("Guy's office Hours text has been changed");
        
        ComboBox cb = (ComboBox)gui.getGUINode(CSG_SUBJECT_COMBO_BOX);
        cb.getValue();
        //System.out.println(cb.getItems().contains("option 1"));
    }
    
    public void processChangeTitle(String classInitials){
        AppGUIModule gui = app.getGUIModule();
        TextField titleTF = (TextField) gui.getGUINode(CSG_TITLE_TEXT_FIELD);
        ComboBox subjectCB = (ComboBox) gui.getGUINode(CSG_SUBJECT_COMBO_BOX);
        System.out.println(classInitials);
        
        if(classInitials.equals("CSE")){
            titleTF.setText("Computer Science");
        }
        else if(classInitials.equals("MAT")){
            titleTF.setText("Mathematics");
        }
        else if(classInitials.equals("AMS")){
            titleTF.setText("Applied Math");
        }
        else
            titleTF.setText(subjectCB.getEditor().getText());
    }
    
    
//TAB 2 FUNCTIONALITY 
    public void changeDescription(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        TextArea description = (TextArea) gui.getGUINode(CSG_DESCRIPTION_TEXT_AREA);
        
        data.setDesc(description.getText());
        
    }
    
    public void changeTopics(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        TextArea words = (TextArea) gui.getGUINode(CSG_TOPICS_TEXT_AREA);
        
        data.setTopics(words.getText());
        
    }
    
    public void changePrereqs(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        TextArea words = (TextArea) gui.getGUINode(CSG_PREREQUISITES_TEXT_AREA);
        
        data.setPrereqs(words.getText());
        
    }
    
    public void changeOutcomes(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        TextArea outcomes = (TextArea) gui.getGUINode(CSG_OUTCOMES_TEXT_AREA);
        
        data.setOutcomes(outcomes.getText());
        
    }
    
    public void changeTextbooks(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        TextArea textbooks = (TextArea) gui.getGUINode(CSG_TEXTBOOKS_TEXT_AREA);
        
        data.setTextbooks(textbooks.getText());
        
    }
    
    public void changeGradedComponents(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        TextArea gradedComponents = (TextArea) gui.getGUINode(CSG_GRADED_COMPONENTS_TEXT_AREA);
        
        data.setGradedComponents(gradedComponents.getText());
        
    }
    
    public void changeGradingNote(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        TextArea gradingNote = (TextArea) gui.getGUINode(CSG_GRADING_NOTE_TEXT_AREA);
        
        data.setGradingNote(gradingNote.getText());
        
    }
    
    public void changeAcademicDishonesty(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        TextArea academicDishonesty = (TextArea) gui.getGUINode(CSG_ACADEMIC_DISHONESTY_TEXT_AREA);
        
        data.setAcademicDishonesty(academicDishonesty.getText());
        System.out.println(data.getAcademicDishonesty());
    }
    
    public void changeSpecialAssistance(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        TextArea specialAssistance = (TextArea) gui.getGUINode(CSG_SPECIAL_ASSISTANCE_TEXT_AREA);
        
        data.setSpecialAssistance(specialAssistance.getText());
        
    }
    
    
//TAB 4 FUNCTIONALITY
    public void processAddTA() {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView) gui.getGUINode(CSG_TAS_TABLE_VIEW);
        TableView rightTable = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        RadioButton ugradButton = (RadioButton) gui.getGUINode(CSG_UNDERGRADUATE_RADIO_BUTTON);
        RadioButton gradButton = (RadioButton) gui.getGUINode(CSG_GRADUATE_RADIO_BUTTON);
        RadioButton allButton = (RadioButton) gui.getGUINode(CSG_ALL_RADIO_BUTTON);
        TextField nameTF = (TextField) gui.getGUINode(CSG_TA_NAME_TEXT_FIELD);
        Button addTAButton = (Button) gui.getGUINode(CSG_ADD_TA_BUTTON);
        String name = nameTF.getText();
        TextField emailTF = (TextField) gui.getGUINode(CSG_TA_EMAIL_TEXT_FIELD);
        String email = emailTF.getText();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();

        if (data.isLegalNewTA(name, email)) {
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name.trim(), email.trim());
            if(ugradButton.isSelected()){
                ta.setType("Undergraduate");
            }
            else if(gradButton.isSelected()){
                ta.setType("Graduate");
            }
            else
                System.out.println("neither valid button is selected");
            
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);

            // NOW CLEAR THE TEXT FIELDS
            nameTF.setText("");
            emailTF.setText("");
            nameTF.requestFocus();
        }
        tasTable.refresh();
        rightTable.refresh();
    }
    
    public void processAddTypeTA(String type) {
        AppGUIModule gui = app.getGUIModule();
        TextField nameTF = (TextField) gui.getGUINode(CSG_TA_NAME_TEXT_FIELD);
        String name = nameTF.getText();
        TextField emailTF = (TextField) gui.getGUINode(CSG_TA_EMAIL_TEXT_FIELD);
        String email = emailTF.getText();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        if (data.isLegalNewTA(name, email)) {
            //nameTF.setStyle("-fx-text-fill: blue");
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name.trim(), email.trim(), type.trim());
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);

            // NOW CLEAR THE TEXT FIELDS
            nameTF.setText("");
            emailTF.setText("");
            nameTF.requestFocus();
        }
    }
    

    public void processVerifyTA() {

    }

    public void processToggleOfficeHours() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TablePosition> selectedCells = officeHoursTableView.getSelectionModel().getSelectedCells();
        if (selectedCells.size() > 0) {
            TablePosition cell = selectedCells.get(0);
            int cellColumnNumber = cell.getColumn();
            CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
            if (data.isDayOfWeekColumn(cellColumnNumber)) {
                DayOfWeek dow = data.getColumnDayOfWeek(cellColumnNumber);
                TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(CSG_TAS_TABLE_VIEW);
                TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
                if (ta != null) {
                    TimeSlot timeSlot = officeHoursTableView.getSelectionModel().getSelectedItem();
                    ToggleOfficeHours_Transaction transaction = new ToggleOfficeHours_Transaction(data, timeSlot, dow, ta);
                    app.processTransaction(transaction);
                }
                else {
                    Stage window = app.getGUIModule().getWindow();
                    AppDialogsFacade.showMessageDialog(window, CSG_NO_TA_SELECTED_TITLE, CSG_NO_TA_SELECTED_CONTENT);
                }
            }
            int row = cell.getRow();
            cell.getTableView().refresh();
        }
    }

    public void processTypeTA() {
        app.getFoolproofModule().updateControls(CSG_FOOLPROOF_SETTINGS);
    }
    
    public void printAllTas(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData dataManager = (CourseSiteGeneratorData) app.getDataComponent();
        ObservableList<TeachingAssistantPrototype> all = dataManager.getTaList();
        for(TeachingAssistantPrototype ta: all){
            System.out.println(ta.getName().toString() + " is of type " + ta.getType());
        }
        System.out.println("ends here \n");
    }
    
    public void processShowTaTable(String type){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData dataManager = (CourseSiteGeneratorData) app.getDataComponent();
        TableView<TeachingAssistantPrototype> leftTable = (TableView) gui.getGUINode(CSG_TAS_TABLE_VIEW);
        TableView<TimeSlot> rightTable = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        Button addTa = (Button) gui.getGUINode(CSG_ADD_TA_BUTTON);
        
        RadioButton allButton = (RadioButton) gui.getGUINode(CSG_ALL_RADIO_BUTTON);
        RadioButton ugButton = (RadioButton) gui.getGUINode(CSG_UNDERGRADUATE_RADIO_BUTTON);
        RadioButton grButton = (RadioButton) gui.getGUINode(CSG_GRADUATE_RADIO_BUTTON);
        
        ObservableList<TeachingAssistantPrototype> all = dataManager.getTaList();
        ObservableList<TeachingAssistantPrototype> ug = FXCollections.observableArrayList();
        ObservableList<TeachingAssistantPrototype> gr = FXCollections.observableArrayList();
        //gr = FXCollections.sort();
        if(type.equals("all")){
            addTa.setDisable(ENABLED);
            System.out.println(""); 
            System.out.println("List of all TAs");
            for(TeachingAssistantPrototype ta: all){
                System.out.println(ta.getName());
            }
            
            leftTable.setItems(all);
            
            Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
            while (timeSlotsIterator.hasNext()) {
                TimeSlot timeSlot = timeSlotsIterator.next();
                for (int i = 0; i < DayOfWeek.values().length; i++) {
                    DayOfWeek dow = DayOfWeek.values()[i];
                    if(timeSlot.getDayText().get(dow) != null){
                        StringProperty names = timeSlot.getDayText().get(dow);
                        names.setValue("");
                        ArrayList<TeachingAssistantPrototype> tas = timeSlot.getBackEndTa().get(dow);
                        
                        for(TeachingAssistantPrototype ta: tas){
                            if(all.contains(ta)){
                                names.setValue(names.getValue()+ta.getName());
                            }
                        }
                    }
                    else
                        continue;
                }
            }
            
        //    JsonArray officeHoursArray = officeHoursArrayBuilder.build();
//        Iterator<String> subjectOptionsIterator = dataManager.subjectIterator();
//        while(subjectOptionsIterator.hasNext()){
//            System.out.println(subjectOptionsIterator.next());
//        }
        
        
            leftTable.refresh();
        }
        else if(type.equals("ug")){
            addTa.setDisable(false);
            processAddTA();
            //for(TeachingAssistantPrototype ta: taTableView.getItems().){
            for(TeachingAssistantPrototype ta: all){
                if(ta.getType().equals("Undergraduate")){
                    System.out.println(ta.getName() + " is Undergraduate");
                    ug.add(ta);
                }
            }
            leftTable.setItems(ug);
            leftTable.refresh();
            
            Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
            while (timeSlotsIterator.hasNext()) {
                TimeSlot timeSlot = timeSlotsIterator.next();
                for (int i = 0; i < DayOfWeek.values().length; i++) {
                    DayOfWeek dow = DayOfWeek.values()[i];
                    if(timeSlot.getDayText().get(dow) != null){
                        StringProperty names = timeSlot.getDayText().get(dow);
                        names.setValue("");
                        ArrayList<TeachingAssistantPrototype> tas = timeSlot.getBackEndTa().get(dow);
                        
                        for(TeachingAssistantPrototype ta: tas){
                            if(ug.contains(ta)){
                                names.setValue(names.getValue()+ta.getName());
                            }
                        }
                    }
                    else
                        continue;
                }
            }
            leftTable.refresh();
            rightTable.refresh();
        }
        else if(type.equals("gr")){
            addTa.setDisable(false);
            processAddTA();
            for(TeachingAssistantPrototype ta: all){
                if(ta.getType().equals("Graduate")){
                    gr.add(ta);
                    System.out.println(ta.getName() + " is Graduate");
                }
            }
            leftTable.setItems(gr);
            leftTable.refresh();
            System.out.println("\n");
            
            
            //FIXING THE RIGHT TABLE SO IT MATCHES WHATEVER IS IN THE RIGHT TABLE
            Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
            while (timeSlotsIterator.hasNext()) {
                TimeSlot timeSlot = timeSlotsIterator.next();
                for (int i = 0; i < DayOfWeek.values().length; i++) {
                    DayOfWeek dow = DayOfWeek.values()[i];
                    if(timeSlot.getDayText().get(dow) != null){
                        StringProperty names = timeSlot.getDayText().get(dow);
                        names.setValue("");
                        ArrayList<TeachingAssistantPrototype> tas = timeSlot.getBackEndTa().get(dow);
                        
                        for(TeachingAssistantPrototype ta: tas){
                            if(gr.contains(ta)){
                                names.setValue(names.getValue()+ta.getName());
                            }
                        }
                    }
                    else
                        continue;
                }
            }
            //gr = FXCollections.sort;
            leftTable.refresh();
        }
    }
    
    public void processHighlightNames(){
        AppGUIModule gui = app.getGUIModule();
        TableView<TeachingAssistantPrototype> leftTable = (TableView) gui.getGUINode(CSG_TAS_TABLE_VIEW);
        TeachingAssistantPrototype taInCell = leftTable.getSelectionModel().getSelectedItem();
        
        TableView<TimeSlot> rightTable = (TableView) gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);

        CourseSiteGeneratorData dataManager = (CourseSiteGeneratorData)app.getDataComponent();
        //Iterator<TeachingAssistantPrototype> tasIterator = dataManager.teachingAssistantsIterator();
        rightTable.getSelectionModel().clearSelection();
        if (taInCell != null) {
            String taName = taInCell.getName();
            System.out.println(taName);
            
            int timeSlotNumber = 0;
            Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
            while (timeSlotsIterator.hasNext()) {
                TimeSlot timeSlot = timeSlotsIterator.next();
                for (int i = 0; i < DayOfWeek.values().length; i++) {
                    DayOfWeek dow = DayOfWeek.values()[i];
                    if(timeSlot.getDayText().get(dow) != null){
                        StringProperty names = timeSlot.getDayText().get(dow);
                        if(names.get().toLowerCase().contains(taName.toLowerCase())){
                            //System.out.println(dow + "contains");
                            //THIS IS THE CELL THAT SHOULD BE COLLORED
                            //names.setValue("lalala");
                            rightTable.getSelectionModel().select(timeSlotNumber, rightTable.getColumns().get(i+2));
                        }
                        else{
                            //System.out.println(dow + " does not contain " +taName);
                        }
                    }
                    else
                        continue;
                }
                timeSlotNumber++;
            }
        rightTable.refresh();
        }
        else {
            Stage window = app.getGUIModule().getWindow();
            AppDialogsFacade.showMessageDialog(window, CSG_NO_TA_SELECTED_TITLE, CSG_NO_TA_SELECTED_CONTENT);
        }
    }
    
    
    public void processEdit(){
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        TableView<TeachingAssistantPrototype> leftTable = (TableView) gui.getGUINode(CSG_TAS_TABLE_VIEW);
        TeachingAssistantPrototype taInCell = leftTable.getSelectionModel().getSelectedItem();
        String taName = taInCell.getName();
        String taEmail = taInCell.getEmail();
        String taType = taInCell.getType();
        
        Dialog<ButtonType> editTA = new Dialog<>();
        editTA.setTitle("Edit " + taName);
        editTA.setHeaderText("Edit " +taName);
        
        ButtonType makeChanges = new ButtonType("MAKE CHANGES", ButtonBar.ButtonData.OK_DONE);
        editTA.getDialogPane().getButtonTypes().addAll(makeChanges, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10,10,10,10));
        
        TextField newName = new TextField();
        newName.setPromptText("Name");
        newName.setText(taName);
        TextField newEmail = new TextField();
        newEmail.setPromptText("Email");
        newEmail.setText(taEmail);
        
        
        final ToggleGroup group = new ToggleGroup();
        RadioButton ug = new RadioButton();
        ug.setText("Undergraduate");
        ug.setToggleGroup(group);
        RadioButton gr = new RadioButton();
        gr.setText("Graduate");
        gr.setToggleGroup(group);
        //RadioButton third = new RadioButton("Example");  // Alternative way of declaring button
        if(taType.equals("Undergraduate")){
            ug.setSelected(ENABLED);
            ug.requestFocus();
        }
        else{
            gr.setSelected(ENABLED);
            gr.requestFocus();
        }

        grid.add(new Label("Name: "),0,0);
        grid.add(newName, 1, 0);
        grid.add(new Label("Email: "),0,1);
        grid.add(newEmail, 1, 1);
        grid.add(new Label("Type: "), 0, 2);
        grid.add(ug, 1, 2);
        grid.add(gr, 2, 2);

       
        //Node editButton = editTA.getDialogPane().lookupButton(ButtonType.CLOSE);
        //editButton.setDisable(true);
        
        editTA.getDialogPane().setContent(grid);

        newName.textProperty().addListener(e ->{
            processTypeTA();
        });
        newEmail.textProperty().addListener(e ->{
            processTypeTA();
        });
        
        /**
         * 
         * ADD RED DESIGN IF NOT VALID NAME AND EMAIL
         */
        
        String type = "";
        Optional<ButtonType> result = editTA.showAndWait();
        if(result.get()==makeChanges){
            System.out.println("Make Changes Selected");
            System.out.println("Name before change: " + taInCell.getName());
            //taInCell.setName(newName.getText());
            System.out.println("Name after: " + taInCell.getName());
            //taInCell.setEmail(newEmail.getText());
            if(ug.isSelected()){
                System.out.println("Undergrad selected");
                //taInCell.setType("Undergraduate");
                type = "Undergraduate";
            }
            else if(gr.isSelected()){
                System.out.println("Grad selected");
                //taInCell.setType("Graduate");
                type = "Graduate";
            }
        }
        else{
            System.out.println("Canceled Changes");
        }
        EditTA_Transaction editTATransaction = new EditTA_Transaction(data, taInCell, newName.getText(), newEmail.getText(), type);
        app.processTransaction(editTATransaction);
        leftTable.refresh();
    }
    
    
    
    public void cutTA(){
        AppGUIModule gui = app.getGUIModule();
        
    }
    public void copyTA(){
        AppGUIModule gui = app.getGUIModule();
        
    }
    public void pasteTA(){
        AppGUIModule gui = app.getGUIModule();
        
    }
    
    //Tab 4 Function - Remove TA
    public void removeTA() {
        
    }

    public void checkSubjectSelection() {
        CourseSiteGeneratorData data = (CourseSiteGeneratorData) app.getDataComponent();
        System.out.println(data.selectedSubject());
        System.out.println("Hello- made it here");
    }

    
    
}
