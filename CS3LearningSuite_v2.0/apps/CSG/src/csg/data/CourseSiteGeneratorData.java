/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGeneratorApp;
import csg.data.TimeSlot.DayOfWeek;
import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.ObservableList;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import static csg.CourseSiteGeneratorPropertyType.*;
import static csg.data.TAType.Graduate;
import javafx.collections.FXCollections;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Galen
 */
public class CourseSiteGeneratorData implements AppDataComponent {
    CourseSiteGeneratorApp app;
    ObservableList<TeachingAssistantPrototype> teachingAssistants;
    ObservableList<TimeSlot> officeHours;
    
    ObservableList<Labs> labs;
    ObservableList<Lectures> lectures;
    ObservableList<Recitations> recitations;
    
    //Banner
    //SubjectOptions
    ObservableList<String> subjectOptions;
    ObservableList<String> numberOptions;
    ObservableList<String> semesterOptions;
    ObservableList<String> yearOptions;
    
    
    
    Instructor guy;
    String instructorName;
    String instructorRoom;
    String instructorEmail;
    String instructorHomePage;
    String instructorHours;
    
    String title;
    String exportDir;
    
    String description;
    String topics;
    String prerequisites;
    String outcomes;
    String textbooks;
    String gradedComponents;
    String gradingNote;
    String academicDishonesty;
    String specialAssistance;

    // THESE ARE THE TIME BOUNDS FOR THE OFFICE HOURS GRID. NOTE
    // THAT THESE VALUES CAN BE DIFFERENT FOR DIFFERENT FILES, BUT
    // THAT OUR APPLICATION USES THE DEFAULT TIME VALUES AND PROVIDES
    // NO MEANS FOR CHANGING THESE VALUES
    int startHour;
    int endHour;
    
    // DEFAULT VALUES FOR START AND END HOURS IN MILITARY HOURS
    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;

    /**
     * This constructor will setup the required data structures for
     * use, but will have to wait on the office hours grid, since
     * it receives the StringProperty objects from the Workspace.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public CourseSiteGeneratorData(CourseSiteGeneratorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        AppGUIModule gui = app.getGUIModule();

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(CSG_TAS_TABLE_VIEW);
        teachingAssistants = taTableView.getItems();
            
        //Banner Info Items
        ComboBox subject = (ComboBox) gui.getGUINode(CSG_SUBJECT_COMBO_BOX);
        subjectOptions = subject.getItems();
        
        ComboBox number = (ComboBox) gui.getGUINode(CSG_NUMBER_COMBO_BOX);
        numberOptions = number.getItems();
        
        ComboBox semester = (ComboBox) gui.getGUINode(CSG_SEMESTER_COMBO_BOX);
        semesterOptions = semester.getItems();
        
        ComboBox year = (ComboBox) gui.getGUINode(CSG_YEAR_COMBO_BOX);
        yearOptions = year.getItems();
        
        TextArea textArea = (TextArea) gui.getGUINode(CSG_INSTRUCTOR_OFFICE_HOURS);
        instructorHours = textArea.getText();
        
        TextField nameTF = (TextField) gui.getGUINode(CSG_INSTRUCTOR_NAME_TEXT_FIELD);
        TextField emailTF = (TextField) gui.getGUINode(CSG_INSTRUCTOR_EMAIL_TEXT_FIELD);
        TextField roomTF = (TextField) gui.getGUINode(CSG_INSTRUCTOR_ROOM_TEXT_FIELD);
        TextField homePageTF = (TextField) gui.getGUINode(CSG_INSTRUCTOR_HOME_PAGE_TEXT_FIELD);
        
        guy = new Instructor(nameTF.getText(),emailTF.getText(),roomTF.getText(),homePageTF.getText());
        guy.setOfficeHours(instructorHours);
        
        TextField titleTF = (TextField) gui.getGUINode(CSG_TITLE_TEXT_FIELD);
        //titleTF.setText(title);
        title = titleTF.getText();
        
        Label exportDirLabel = (Label) gui.getGUINode(CSG_EXPORT_DIR_LABEL);
        exportDir = exportDirLabel.getText();
        
        //SYLLABUS DATA
        TextArea descriptionText = (TextArea) gui.getGUINode(CSG_DESCRIPTION_TEXT_AREA);
        description = descriptionText.getText();
        
        TextArea topicsText = (TextArea) gui.getGUINode(CSG_TOPICS_TEXT_AREA);
        topics = topicsText.getText();
        
        TextArea prerequisitesText = (TextArea) gui.getGUINode(CSG_PREREQUISITES_TEXT_AREA);
        prerequisites = prerequisitesText.getText();
        
        TextArea outcomesText = (TextArea) gui.getGUINode(CSG_OUTCOMES_TEXT_AREA);
        outcomes = outcomesText.getText();
        
        TextArea textbooksText = (TextArea) gui.getGUINode(CSG_TEXTBOOKS_TEXT_AREA);
        textbooks = textbooksText.getText();
        
        TextArea gradedComponentsText = (TextArea) gui.getGUINode(CSG_GRADED_COMPONENTS_TEXT_AREA);
        gradedComponents = gradedComponentsText.getText();
        
        TextArea gradingNoteText = (TextArea) gui.getGUINode(CSG_GRADING_NOTE_TEXT_AREA);
        gradingNote = gradingNoteText.getText();
        
        TextArea academicDishonestyText = (TextArea) gui.getGUINode(CSG_ACADEMIC_DISHONESTY_TEXT_AREA);
        academicDishonesty = academicDishonestyText.getText();
        
        TextArea specialAssistanceText = (TextArea) gui.getGUINode(CSG_SPECIAL_ASSISTANCE_TEXT_AREA);
        specialAssistance = specialAssistanceText.getText();
        
        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        
        resetOfficeHours();
    }
    
//TAB 1 HELPER METHODS
    public String getInstructorHours(){
        return instructorHours;
    }
    public Instructor getInstructor(){
        return guy;
    }
    
    public void changeInstructorHours(){
        System.out.println("Process the change instructor hours methods. WORDS HAVE BEEN ADDED");
    }
    
    public void unchangeInstructorHours(){
        System.out.println("Process the UNchange instructor hours methods. DELETE THESE WORDS");
    }
    
    public void changeComboBox(){
        System.out.println("Combo box change");
    }
    
    public void unchangeComboBox(){
        System.out.println("Undo combo box change");
    }
    
    public void checkBox(){
        System.out.println("Check the box");
    }
    
    public void uncheckBox(){
        System.out.println("Uncheck the box");
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public String getExportDir(){
        return this.exportDir;
    }
    
    public void setDesc(String initString){
        this.description = initString;
    }
    
    public void setTopics(String initString){
        this.topics = initString;
    }
    
    public void setPrereqs(String initString){
        this.prerequisites = initString;
    }
    
    public void setOutcomes(String initString){
        this.outcomes = initString;
    }
    
    public void setTextbooks(String initString){
        this.textbooks = initString;
    }
    
    public void setGradedComponents(String initString){
        this.gradedComponents = initString;
    }
    
    public void setGradingNote(String initString){
        this.gradingNote = initString;
    }
    
    public void setAcademicDishonesty(String initString){
        this.academicDishonesty = initString;
    }
    
    public void setSpecialAssistance(String initString){
        this.specialAssistance = initString;
    }
    
    public String getDesc(){
        return this.description;
    }
    
    public String getTopics(){
        return this.topics;
    }
    
    public String getPrereqs(){
        return this.prerequisites;
    }
    
    public String getOutcomes(){
        return this.outcomes;
    }
    
    public String getTextBooks(){
        return this.textbooks;
    }
    
    public String getGradedComponents(){
        return this.gradedComponents;
    }
    
    public String getGradingNote(){
        return this.gradingNote;
    }
    
    public String getAcademicDishonesty(){
        return this.academicDishonesty;
    }
    
    public String getSpecialAssistance(){
        return this.specialAssistance;
    }
    
    public ObservableList<String> getSubjectOptions(){
        return this.subjectOptions;
    }
    
    public void setSubjectOptions(ObservableList<String> initSubjectOptions){
        this.subjectOptions = initSubjectOptions;
    }
    
    public ObservableList<String> getNumberOptions(){
        return this.numberOptions;
    }
    
    public void setNumberOptions(ObservableList<String> initNumberOptions){
        this.numberOptions = initNumberOptions;
    }
    
 //TAB 4 HELPER METHODS   
    private void resetOfficeHours() {
        //THIS WILL STORE OUR OFFICE HOURS
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView)gui.getGUINode(CSG_OFFICE_HOURS_TABLE_VIEW);
        officeHours = officeHoursTableView.getItems(); 
        officeHours.clear();
        for (int i = startHour; i <= endHour; i++) {
            TimeSlot timeSlot = new TimeSlot(   this.getTimeString(i, true),
                                                this.getTimeString(i, false));
            officeHours.add(timeSlot);
            
            TimeSlot halfTimeSlot = new TimeSlot(   this.getTimeString(i, false),
                                                    this.getTimeString(i+1, true));
            officeHours.add(halfTimeSlot);
        }
    }
    
    public ObservableList<TeachingAssistantPrototype> getTaList (){
        return this.teachingAssistants;
    }
    
    private String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if (initStartHour <= initEndHour) {
            // THESE ARE VALID HOURS SO KEEP THEM
            // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
            startHour = initStartHour;
            endHour = initEndHour;
        }
        resetOfficeHours();
    }
        
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    
    @Override
    public void reset() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        teachingAssistants.clear();
        
        for (TimeSlot timeSlot : officeHours) {
            timeSlot.reset();
        }
        
        instructorName = "";
        instructorEmail = "";
        guy.setName("");
        subjectOptions.clear();
        numberOptions.clear();
        
        AppGUIModule gui = app.getGUIModule();
        CheckBox homeCB = (CheckBox) gui.getGUINode(CSG_HOME_CHECK_BOX);
        homeCB.setSelected(false);
        CheckBox syllabusCB = (CheckBox) gui.getGUINode(CSG_SYLLABUS_CHECK_BOX);
        syllabusCB.setSelected(false);
        CheckBox scheduleCB = (CheckBox) gui.getGUINode(CSG_SCHEDULE_CHECK_BOX);
        scheduleCB.setSelected(false);
        CheckBox homeworkCB = (CheckBox) gui.getGUINode(CSG_HOMEWORK_CHECK_BOX);
        homeworkCB.setSelected(false);
        
        TextField inName = (TextField) gui.getGUINode(CSG_INSTRUCTOR_NAME_TEXT_FIELD);
        inName.clear();
        TextField inEmail = (TextField) gui.getGUINode(CSG_INSTRUCTOR_EMAIL_TEXT_FIELD);
        inEmail.clear();
        TextField inRoom = (TextField) gui.getGUINode(CSG_INSTRUCTOR_ROOM_TEXT_FIELD);
        inRoom.clear();
        TextField inHomePage = (TextField) gui.getGUINode(CSG_INSTRUCTOR_HOME_PAGE_TEXT_FIELD);
        inHomePage.clear();
        
        TextArea inHours = (TextArea) gui.getGUINode(CSG_INSTRUCTOR_OFFICE_HOURS);
        inHours.clear();
        
        TextArea desc = (TextArea) gui.getGUINode(CSG_DESCRIPTION_TEXT_AREA);
        desc.clear();
        TextArea topic = (TextArea) gui.getGUINode(CSG_TOPICS_TEXT_AREA);
        topic.clear();
        TextArea pre = (TextArea) gui.getGUINode(CSG_PREREQUISITES_TEXT_AREA);
        pre.clear();
        TextArea out = (TextArea) gui.getGUINode(CSG_OUTCOMES_TEXT_AREA);
        out.clear();
        TextArea tb = (TextArea) gui.getGUINode(CSG_TEXTBOOKS_TEXT_AREA);
        tb.clear();
        TextArea gc = (TextArea) gui.getGUINode(CSG_GRADED_COMPONENTS_TEXT_AREA);
        gc.clear();
        TextArea gn = (TextArea) gui.getGUINode(CSG_GRADING_NOTE_TEXT_AREA);
        gn.clear();
        TextArea ad = (TextArea) gui.getGUINode(CSG_ACADEMIC_DISHONESTY_TEXT_AREA);
        ad.clear();
        
        
        
    }
    
    // ACCESSOR METHODS

    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }
    
    public boolean isTASelected() {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView)gui.getGUINode(CSG_TAS_TABLE_VIEW);
        return tasTable.getSelectionModel().getSelectedItem() != null;
    }
    
    public void addTA(TeachingAssistantPrototype ta) {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView)gui.getGUINode(CSG_TAS_TABLE_VIEW);
        if (!this.teachingAssistants.contains(ta))
            this.teachingAssistants.add(ta);
        tasTable.refresh();
    }
    
    public void removeTA(TeachingAssistantPrototype ta) {
        // REMOVE THE TA FROM THE LIST OF TAs
        this.teachingAssistants.remove(ta);
        
        // AND REMOVE THE TA FROM ALL THEIR OFFICE HOURS
    }
    
    public void cutTA(TeachingAssistantPrototype ta){
        
    }
    
    public void uncutTA(TeachingAssistantPrototype ta){
        
    }

    public boolean isLegalNewTA(String name, String email) {
        if ((name.trim().length() > 0)
                && (email.trim().length() > 0)) {
            // MAKE SURE NO TA ALREADY HAS THE SAME NAME
            TeachingAssistantPrototype testTA = new TeachingAssistantPrototype(name, email);
            
            if (this.teachingAssistants.contains(testTA))
                return false;
            if (this.isLegalNewEmail(email)) {
                //System.out.println("Pass legal TA check");
                return true;
            }
        }
        return false;
    }
    
    public boolean isLegalName(String name){
        if((name.trim().length()> 0)){
            for(TeachingAssistantPrototype ta: this.teachingAssistants){
                if(ta.getName().toLowerCase().equals(name.toLowerCase())){   ///MAKE SURE THIS STRING IS LOWERCASE
                    return false;
                }
            }
        }
        return true;
    }
    
    public boolean isLegalNewEmail(String email) {
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
                "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        if (matcher.find()) {
            for (TeachingAssistantPrototype ta : this.teachingAssistants) {
                if (ta.getEmail().equals(email.trim()))
                    return false;
            }
            return true;
        }
        else return false;
    }
    
    public boolean isDayOfWeekColumn(int columnNumber) {
        return columnNumber >= 2;
    }
    
    public DayOfWeek getColumnDayOfWeek(int columnNumber) {
        return TimeSlot.DayOfWeek.values()[columnNumber-2];
    }

    public Iterator<TeachingAssistantPrototype> teachingAssistantsIterator() {
        return teachingAssistants.iterator();
    }
    
    public Iterator<TimeSlot> officeHoursIterator() {
        return officeHours.iterator();
    }
    
    public Iterator<String> subjectIterator(){
        return subjectOptions.iterator();
    }

    public Iterator<String> numberIterator(){
        return numberOptions.iterator();
    }

    public Iterator<String> semesterIterator(){
        return semesterOptions.iterator();
    }

    public Iterator<String> yearIterator(){
        return yearOptions.iterator();
    }
    
    public String selectedSubject(){
        AppGUIModule gui = app.getGUIModule();
        ComboBox subject = (ComboBox) gui.getGUINode(CSG_SUBJECT_COMBO_BOX);
        return subject.getSelectionModel().getSelectedItem().toString();
    }

    
    public TeachingAssistantPrototype getTAWithName(String name) {
        Iterator<TeachingAssistantPrototype> taIterator = teachingAssistants.iterator();
        while (taIterator.hasNext()) {
            TeachingAssistantPrototype ta = taIterator.next();
            if (ta.getName().equals(name))
                return ta;
        }
        return null;
    }

    public TeachingAssistantPrototype getTAWithEmail(String email) {
        Iterator<TeachingAssistantPrototype> taIterator = teachingAssistants.iterator();
        while (taIterator.hasNext()) {
            TeachingAssistantPrototype ta = taIterator.next();
            if (ta.getEmail().equals(email))
                return ta;
        }
        return null;
    }

    public TimeSlot getTimeSlot(String startTime) {
        Iterator<TimeSlot> timeSlotsIterator = officeHours.iterator();
        while (timeSlotsIterator.hasNext()) {
            TimeSlot timeSlot = timeSlotsIterator.next();
            String timeSlotStartTime = timeSlot.getStartTime().replace(":", "_");
            if (timeSlotStartTime.equals(startTime))
                return timeSlot;
        }
        return null;
    }

    public void editTA(TeachingAssistantPrototype ta, String newName, String newEmail, String newType) {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView)gui.getGUINode(CSG_TAS_TABLE_VIEW);
        tasTable.refresh();
        TeachingAssistantPrototype editTa;
        if (this.teachingAssistants.contains(ta)){
            editTa = teachingAssistants.get(teachingAssistants.indexOf(ta));
            editTa.setName(newName);
            editTa.setEmail(newEmail);
            editTa.setType(newType);
        }
        else
            System.out.println("Failed to edit");
        
    }

    public void uneditTA(TeachingAssistantPrototype ta, String oldName, String oldEmail, String oldType) {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView)gui.getGUINode(CSG_TAS_TABLE_VIEW);
        tasTable.refresh();
        TeachingAssistantPrototype editTa;
        if (this.teachingAssistants.contains(ta)){
            editTa = teachingAssistants.get(teachingAssistants.indexOf(ta));
            editTa.setName(oldName);
            editTa.setEmail(oldEmail);
            editTa.setType(oldType);
        }
        else
            System.out.println("Failed unedit");
    }

    public void editTest(String name) {
        //String name = taToEdit.getName();
        //System.out.println(taToEdit.getEmail());
        System.out.println(name);
    }
}
