package csg.files;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import csg.CourseSiteGeneratorApp;
import static csg.CourseSiteGeneratorPropertyType.*;
import csg.data.CourseSiteGeneratorData;
import csg.data.Instructor;
import csg.data.TeachingAssistantPrototype;
import csg.data.TimeSlot;
import csg.data.TimeSlot.DayOfWeek;
import djf.modules.AppGUIModule;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * This class serves as the file component for the TA
 * manager app. It provides all saving and loading 
 * services for the application.
 * 
 * @author Richard McKenna
 */
public class CourseSiteGeneratorFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    CourseSiteGeneratorApp app;
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_GRAD_TAS = "grad_tas";
    static final String JSON_NAME = "name";
    static final String JSON_EMAIL = "email";
    static final String JSON_TYPE = "type";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_START_TIME = "time";
    static final String JSON_DAY_OF_WEEK = "day";
    static final String JSON_MONDAY = "monday";
    static final String JSON_TUESDAY = "tuesday";
    static final String JSON_WEDNESDAY = "wednesday";
    static final String JSON_THURSDAY = "thursday";
    static final String JSON_FRIDAY = "friday";

    //SITE TAB JSON TYPES
    //Banner JSONs
    static final String JSON_BANNER_SUBJECT_OPTIONS = "subject_options";
    static final String JSON_BANNER_SUBJECT_OPTION = "subject_option";
    static final String JSON_BANNER_SUBJECT_SELECTED = "selected_subject";
    static final String JSON_BANNER_SEMESTER_OPTIONS = "semester_options";
    static final String JSON_BANNER_SEMESTER_OPTION = "semester_option";
    static final String JSON_BANNER_SEMESTER_SELECTED = "selected_semester";
    static final String JSON_BANNER_NUMBER_OPTIONS = "number_options";
    static final String JSON_BANNER_NUMBER_OPTION = "number_option";
    static final String JSON_BANNER_NUMBER_SELECTED = "selected_number";
    static final String JSON_BANNER_YEAR_OPTIONS = "year_options";
    static final String JSON_BANNER_YEAR_OPTION = "year_option";
    static final String JSON_BANNER_YEAR_SELECTED = "selected_year";
    static final String JSON_BANNER_TITLE_TEXT = "title_text";
    static final String JSON_BANNER_EXPORT_DIR_TEXT = "export_dir_text";
    
    //Pages JSONs
    static final String JSON_PAGES = "pages";
    static final String JSON_PAGE = "page_name";
    static final String JSON_PAGE_LINK = "link";
    
    //Style JSONs
    static final String JSON_FAVICON = "favicon";
    static final String JSON_NAVBAR = "navbar";
    static final String JSON_LEFT = "left";
    static final String JSON_RIGHT = "right";
    
    //Instructor JSONs
    static final String JSON_INSTRUCTOR_NAME_TEXT = "instructor_name";
    static final String JSON_INSTRUCTOR_EMAIL_TEXT = "instructor_email";
    static final String JSON_INSTRUCTOR_ROOM_TEXT = "instructor_room";
    static final String JSON_INSTRUCTOR_HOME_PAGE_TEXT = "instructor_home_page";
    
    static final String JSON_INSTRUCTOR_OFFICE_HOURS_TEXT = "instructor_office_hours";
    
    //SYLLABUS TAB JSON OBJECTS
    static final String JSON_DESCRIPTION_TEXT = "description_text";
    static final String JSON_TOPICS_TEXT="topic_text";
    static final String JSON_PREREQUISITES_TEXT="prerequisites_text";
    static final String JSON_OUTCOMES_TEXT="outcomes_text";
    static final String JSON_TEXTBOOKS_TEXT="textbooks_text";
    static final String JSON_GRADED_COMPONENTS_TEXT="graded_components_text";
    static final String JSON_GRADING_NOTE_TEXT="grading_note_text";
    static final String JSON_ACADEMIC_DISHONESTY_TEXT="academic_dishonesty_text";
    static final String JSON_SPECIAL_ASSISTANCE_TEXT="special_assistance_text";
    
    public CourseSiteGeneratorFiles(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	CourseSiteGeneratorData dataManager = (CourseSiteGeneratorData)data;
        dataManager.reset();

	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);

	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);

        
        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            String type = jsonTA.getString(JSON_TYPE);
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name, email, type);
            dataManager.addTA(ta);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String startTime = jsonOfficeHours.getString(JSON_START_TIME);
            DayOfWeek dow = DayOfWeek.valueOf(jsonOfficeHours.getString(JSON_DAY_OF_WEEK));
            String name = jsonOfficeHours.getString(JSON_NAME);
            TeachingAssistantPrototype ta = dataManager.getTAWithName(name);
            TimeSlot timeSlot = dataManager.getTimeSlot(startTime);
            timeSlot.toggleTA(dow, ta);
        }
       
        AppGUIModule gui = app.getGUIModule();
        
//TAB 1 - SITE INFORMATION
    //BANNER INFORMATION
    //SUBJECT COMBO BOX
        ObservableList<String> subjectOptions = FXCollections.observableArrayList();
        JsonArray jsonSubjectArray = json.getJsonArray(JSON_BANNER_SUBJECT_OPTIONS);
        ObservableList<String> alreadySubjects = dataManager.getSubjectOptions();
        for(int i=0; i< jsonSubjectArray.size() ; i++){
            JsonObject jsonOption = jsonSubjectArray.getJsonObject(i);
            if(!alreadySubjects.contains(jsonOption.getString(JSON_BANNER_SUBJECT_OPTION)))
                subjectOptions.add(jsonOption.getString(JSON_BANNER_SUBJECT_OPTION));
        }
        ComboBox subjectOptionsCB = (ComboBox) gui.getGUINode(CSG_SUBJECT_COMBO_BOX);
        subjectOptionsCB.getItems().addAll(subjectOptions);
        String selected = json.getJsonString(JSON_BANNER_SUBJECT_SELECTED).getString();
        dataManager.setSubjectOptions(subjectOptions);
        subjectOptionsCB.setValue(selected);
        
        
    //NUMBER COMBO BOX
        ObservableList<String> numberOptions = FXCollections.observableArrayList();
        JsonArray jsonNumberArray = json.getJsonArray(JSON_BANNER_NUMBER_OPTIONS);
        ObservableList<String> alreadyNumbers = dataManager.getNumberOptions();
        for(int i=0; i< jsonNumberArray.size() ; i++){
            JsonObject jsonOption = jsonNumberArray.getJsonObject(i);
            if(!alreadyNumbers.contains(jsonOption.getString(JSON_BANNER_NUMBER_OPTION)))
                numberOptions.add(jsonOption.getString(JSON_BANNER_NUMBER_OPTION));
        }
        ComboBox numberOptionsCB = (ComboBox) gui.getGUINode(CSG_NUMBER_COMBO_BOX);
        numberOptionsCB.getItems().addAll(numberOptions);
        String selectedNumber = json.getJsonString(JSON_BANNER_NUMBER_SELECTED).getString();
        numberOptionsCB.setValue(selectedNumber);
        
    //Pages Check Boxes
        CheckBox homeCB = (CheckBox) gui.getGUINode(CSG_HOME_CHECK_BOX);
        CheckBox syllabusCB = (CheckBox) gui.getGUINode(CSG_SYLLABUS_CHECK_BOX);
        CheckBox scheduleCB = (CheckBox) gui.getGUINode(CSG_SCHEDULE_CHECK_BOX);
        CheckBox homeworkCB = (CheckBox) gui.getGUINode(CSG_HOMEWORK_CHECK_BOX);
    
        JsonArray pagesArray = json.getJsonArray(JSON_PAGES);
        for(int i=0; i< pagesArray.size(); i++){
            JsonObject pagesOption = pagesArray.getJsonObject(i);
            String page = pagesOption.getString(JSON_PAGE);
            if(page.equals(homeCB.getText()))
                homeCB.setSelected(true);
            else if(page.equals(syllabusCB.getText()))
                syllabusCB.setSelected(true);
            else if(page.equals(scheduleCB.getText()))
                scheduleCB.setSelected(true);
            else if(page.equals(homeworkCB.getText()))
                homeworkCB.setSelected(true);
            else
                System.out.println("Doesnt match at all");
            String link = pagesOption.getString(JSON_PAGE_LINK);
            System.out.println(link + " is being shown");
        }
        
    //INSTRUCTOR INFORMATION  --  
        
        TextField inNameTF = (TextField) gui.getGUINode(CSG_INSTRUCTOR_NAME_TEXT_FIELD);
        String instructorName = json.getString(JSON_INSTRUCTOR_NAME_TEXT);
        dataManager.getInstructor().setName(instructorName);
        inNameTF.setText(instructorName);
        
        TextField inEmailTF = (TextField) gui.getGUINode(CSG_INSTRUCTOR_EMAIL_TEXT_FIELD);
        String instructorEmail = json.getString(JSON_INSTRUCTOR_EMAIL_TEXT);
        dataManager.getInstructor().setEmail(instructorEmail);
        inEmailTF.setText(instructorEmail);
        
        TextField inRoomTF = (TextField) gui.getGUINode(CSG_INSTRUCTOR_ROOM_TEXT_FIELD);
        String instructorRoom = json.getString(JSON_INSTRUCTOR_ROOM_TEXT);
        dataManager.getInstructor().setRoom(instructorRoom);
        inRoomTF.setText(instructorRoom);
        
        TextField inHomePageTF = (TextField) gui.getGUINode(CSG_INSTRUCTOR_HOME_PAGE_TEXT_FIELD);
        String instructorHomePage = json.getString(JSON_INSTRUCTOR_HOME_PAGE_TEXT);
        dataManager.getInstructor().setHomePage(instructorHomePage);
        inHomePageTF.setText(instructorHomePage);
        
        TextArea inOHTF = (TextArea) gui.getGUINode(CSG_INSTRUCTOR_OFFICE_HOURS);
        String instructorOfficeHours = json.getString(JSON_INSTRUCTOR_OFFICE_HOURS_TEXT);
        dataManager.getInstructor().setOfficeHours(instructorOfficeHours);
        inOHTF.setText(instructorOfficeHours);
        
//Tab 2 Loading
        TextArea descriptionArea = (TextArea) gui.getGUINode(CSG_DESCRIPTION_TEXT_AREA);
        String description = json.getString(JSON_DESCRIPTION_TEXT);
        descriptionArea.setText(description);
        dataManager.setDesc(description);
        
        TextArea topicsArea = (TextArea) gui.getGUINode(CSG_TOPICS_TEXT_AREA);
        String topics = json.getString(JSON_TOPICS_TEXT);
        topicsArea.setText(topics);
        dataManager.setTopics(topics);
        
        TextArea prereqsArea = (TextArea) gui.getGUINode(CSG_PREREQUISITES_TEXT_AREA);
        prereqsArea.setText(json.getString(JSON_PREREQUISITES_TEXT));
        dataManager.setPrereqs(json.getString(JSON_PREREQUISITES_TEXT));
        
        TextArea outcomesArea = (TextArea) gui.getGUINode(CSG_OUTCOMES_TEXT_AREA);
        outcomesArea.setText(json.getString(JSON_OUTCOMES_TEXT));
        dataManager.setOutcomes(json.getString(JSON_OUTCOMES_TEXT));
        
        TextArea textbooksArea = (TextArea) gui.getGUINode(CSG_TEXTBOOKS_TEXT_AREA);
        textbooksArea.setText(json.getString(JSON_TEXTBOOKS_TEXT));
        dataManager.setTextbooks(json.getString(JSON_TEXTBOOKS_TEXT));
        
        TextArea gradedComponentsArea = (TextArea) gui.getGUINode(CSG_GRADED_COMPONENTS_TEXT_AREA);
        gradedComponentsArea.setText(json.getString(JSON_GRADED_COMPONENTS_TEXT));
        dataManager.setGradedComponents(json.getString(JSON_GRADED_COMPONENTS_TEXT));
        
        TextArea gradingNoteArea = (TextArea) gui.getGUINode(CSG_GRADING_NOTE_TEXT_AREA);
        gradingNoteArea.setText(json.getString(JSON_GRADING_NOTE_TEXT));
        dataManager.setGradingNote(json.getString(JSON_GRADING_NOTE_TEXT));
        
        TextArea academicDishonestyArea = (TextArea) gui.getGUINode(CSG_ACADEMIC_DISHONESTY_TEXT_AREA);
        academicDishonestyArea.setText(json.getString(JSON_ACADEMIC_DISHONESTY_TEXT));
        dataManager.setAcademicDishonesty(json.getString(JSON_ACADEMIC_DISHONESTY_TEXT));
        
        TextArea specialAssistanceArea = (TextArea) gui.getGUINode(CSG_SPECIAL_ASSISTANCE_TEXT_AREA);
        specialAssistanceArea.setText(json.getString(JSON_SPECIAL_ASSISTANCE_TEXT));
        dataManager.setSpecialAssistance(json.getString(JSON_SPECIAL_ASSISTANCE_TEXT));
        
        
    }
      
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	CourseSiteGeneratorData dataManager = (CourseSiteGeneratorData)data;

	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
	Iterator<TeachingAssistantPrototype> tasIterator = dataManager.teachingAssistantsIterator();
        while (tasIterator.hasNext()) {
            TeachingAssistantPrototype ta = tasIterator.next();
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_TYPE, ta.getType())
                    .build();
	    taArrayBuilder.add(taJson);
	}
	JsonArray undergradTAsArray = taArrayBuilder.build();

	// NOW BUILD THE OFFICE HOURS JSON OBJCTS TO SAVE
	JsonArrayBuilder officeHoursArrayBuilder = Json.createArrayBuilder();
        Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
        while (timeSlotsIterator.hasNext()) {
            TimeSlot timeSlot = timeSlotsIterator.next();
            for (int i = 0; i < DayOfWeek.values().length; i++) {
                DayOfWeek dow = DayOfWeek.values()[i];
                tasIterator = timeSlot.getTAsIterator(dow);
                while (tasIterator.hasNext()) {
                    TeachingAssistantPrototype ta = tasIterator.next();
                    JsonObject tsJson = Json.createObjectBuilder()
                        .add(JSON_START_TIME, timeSlot.getStartTime().replace(":", "_"))
                        .add(JSON_DAY_OF_WEEK, dow.toString())
                        .add(JSON_NAME, ta.getName())
                            .build();
                    officeHoursArrayBuilder.add(tsJson);
                }
            }
	}
	JsonArray officeHoursArray = officeHoursArrayBuilder.build();
        
        
        AppGUIModule gui = app.getGUIModule();
// TAB 1 DATA - SITE INFO FIRST
        
    //BANNER INFORMATION  
        //SUBJECT COMBO BOX
        JsonArrayBuilder subjectOptionsArrayBuilder = Json.createArrayBuilder();
        Iterator<String> subjectOptionsIterator = dataManager.subjectIterator();
        while(subjectOptionsIterator.hasNext()){
            JsonObject sjOp = Json.createObjectBuilder()
                    .add(JSON_BANNER_SUBJECT_OPTION, subjectOptionsIterator.next())
                    .build();
            subjectOptionsArrayBuilder.add(sjOp);
        }
        JsonArray subjectOptionsArray = subjectOptionsArrayBuilder.build();
        
        //NUMBER COMBO BOX
        JsonArrayBuilder numberOptionsArrayBuilder = Json.createArrayBuilder();
        Iterator<String> numberOptionsIterator = dataManager.numberIterator();
        while(numberOptionsIterator.hasNext()){
            JsonObject sjOp = Json.createObjectBuilder()
                    .add(JSON_BANNER_NUMBER_OPTION, numberOptionsIterator.next())
                    .build();
            numberOptionsArrayBuilder.add(sjOp);
        }
        JsonArray numberOptionsArray = numberOptionsArrayBuilder.build();
        
        //SEMESTER COMBO BOX
        
        //YEAR COMBO BOX
        
        //COMBO BOX SELECTED ITEMS
        ComboBox subjectCB = (ComboBox) gui.getGUINode(CSG_SUBJECT_COMBO_BOX);
        String selectedSubject = subjectCB.getValue().toString();
        
        ComboBox numberCB = (ComboBox) gui.getGUINode(CSG_NUMBER_COMBO_BOX);
        String selectedNumber = numberCB.getValue().toString();
        
        //TITLE
        String title = dataManager.getTitle();
        
        //Export Dir
        String exportDir = dataManager.getExportDir();
        
        
    //PAGES CHECK BOX INFORMATION
        JsonArrayBuilder pagesSelectedPageBuilder = Json.createArrayBuilder();
        CheckBox homeChB = (CheckBox) gui.getGUINode(CSG_HOME_CHECK_BOX);
        CheckBox syllabusChB = (CheckBox) gui.getGUINode(CSG_SYLLABUS_CHECK_BOX);
        CheckBox scheduleChB = (CheckBox) gui.getGUINode(CSG_SCHEDULE_CHECK_BOX);
        CheckBox homeworkChB = (CheckBox) gui.getGUINode(CSG_HOMEWORK_CHECK_BOX);
        if(homeChB.isSelected()){
            JsonObject homeOb = Json.createObjectBuilder()
                    .add(JSON_PAGE, homeChB.getText())
                    .add(JSON_PAGE_LINK, "home.html")
                    .build();
            pagesSelectedPageBuilder.add(homeOb);
            
            
        }
        if(syllabusChB.isSelected()){
            JsonObject syllabusOb = Json.createObjectBuilder()
                    .add(JSON_PAGE, syllabusChB.getText())
                    .add(JSON_PAGE_LINK, "syllabus.html")
                    .build();
            pagesSelectedPageBuilder.add(syllabusOb);
        }
        if(scheduleChB.isSelected()){
            JsonObject scheduleOb = Json.createObjectBuilder()
                    .add(JSON_PAGE, scheduleChB.getText())
                    .add(JSON_PAGE_LINK, "schedule.html")
                    .build();
            pagesSelectedPageBuilder.add(scheduleOb);
        }
        if(homeworkChB.isSelected()){
            JsonObject homeworkOb = Json.createObjectBuilder()
                    .add(JSON_PAGE, homeworkChB.getText())
                    .add(JSON_PAGE_LINK, "homework.html")
                    .build();
            pagesSelectedPageBuilder.add(homeworkOb);
        }
        
        JsonArray pagesArray = pagesSelectedPageBuilder.build();
        
        
    
    //INSTRUCTOR DATA  -----
        Instructor instructorToSave = dataManager.getInstructor();
        String name = instructorToSave.getName();
        String room = instructorToSave.getRoom();
        String email = instructorToSave.getEmail();
        String homePage = instructorToSave.getHomePage();
        String officeHours = instructorToSave.getOfficeHours();
        
        
    
    
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                
                //OFFICE HOURS
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, officeHoursArray)
                
            //TAB 1 - SITE
                //Banner Objects
                .add(JSON_BANNER_SUBJECT_OPTIONS, subjectOptionsArray)
                .add(JSON_BANNER_SUBJECT_SELECTED, selectedSubject)
                .add(JSON_BANNER_NUMBER_OPTIONS, numberOptionsArray)
                .add(JSON_BANNER_NUMBER_SELECTED, selectedNumber)
                .add(JSON_BANNER_TITLE_TEXT, title)
                .add(JSON_BANNER_EXPORT_DIR_TEXT, exportDir)
                
                //Page Objects
                .add(JSON_PAGES, pagesArray)
                
                //INSTRUCTOR OBJECTS
                .add(JSON_INSTRUCTOR_NAME_TEXT, instructorToSave.getName())
                .add(JSON_INSTRUCTOR_ROOM_TEXT, instructorToSave.getRoom())
                .add(JSON_INSTRUCTOR_EMAIL_TEXT, instructorToSave.getEmail())
                .add(JSON_INSTRUCTOR_HOME_PAGE_TEXT, instructorToSave.getHomePage())
                .add(JSON_INSTRUCTOR_OFFICE_HOURS_TEXT, instructorToSave.getOfficeHours())
                
            //TAB 2 - SYLLABUS
                .add(JSON_DESCRIPTION_TEXT, dataManager.getDesc())
                .add(JSON_TOPICS_TEXT, dataManager.getTopics())
                .add(JSON_PREREQUISITES_TEXT,dataManager.getPrereqs())
                .add(JSON_OUTCOMES_TEXT, dataManager.getOutcomes())
                .add(JSON_TEXTBOOKS_TEXT, dataManager.getTextBooks())
                .add(JSON_GRADED_COMPONENTS_TEXT, dataManager.getGradedComponents())
                .add(JSON_GRADING_NOTE_TEXT, dataManager.getGradingNote())
                .add(JSON_ACADEMIC_DISHONESTY_TEXT, dataManager.getAcademicDishonesty())
                .add(JSON_SPECIAL_ASSISTANCE_TEXT, dataManager.getSpecialAssistance())
                
                .build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}