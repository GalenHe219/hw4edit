/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.transactions;

import csg.data.CourseSiteGeneratorData;
import jtps.jTPS_Transaction;

/**
 *
 * @author Galen
 */
public class EditComboBox_Transaction implements jTPS_Transaction{
    CourseSiteGeneratorData data; 
    
    public EditComboBox_Transaction(CourseSiteGeneratorData initData){
        data = initData;
    }
    
    
    @Override 
    public void doTransaction(){
        data.changeComboBox();
    }
    
    public void undoTransaction(){
        data.unchangeComboBox();
    }
}
